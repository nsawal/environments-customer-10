README.txt
This scripts directory contains scripts that are needed as part of Avmed postDeploymentActivity.
Currently its has
avmed_last_run_update.sh - This script replaces starttime & endtime placeholders, from in all json files with current date and time.
avmed_bam_updater.sh - This script runs hrc:last-run-info command and set last run date for all the jsons present under $HE_DIR/last-run-info/ directory
he_accumulator_configure.sh - This script initiates the generic accumulator processing
ccpe/insert_crons.sh - inserts required 18 cron entries into the crontab (there is one more cron avmed-cob-coverage-investigation, that requires manual entry into the crontab) 
ccpe/ccpe_run.sh $1 - copies required ccpe scripts and jars into the $KARAF_HOME/ccpe/scripts/ directory.

